import React, { useState, useEffect } from 'react'
import io from 'socket.io-client'

const socket = io('http://localhost:3000')

function Control ({ onScalingChange }) {
    const [isConnected, setIsConnected] = useState(socket.connected)
    const [lastPong, setLastPong] = useState(null)
    const [isShowScalingPopup, setIsShowScalingPopup] = useState(false)
    const [scaling, setScaling] = useState(1)

    useEffect(() => {
        socket.on('connect', () => {
            setIsConnected(true)
        })

        socket.on('disconnect', () => {
            setIsConnected(false)
        })

        socket.on('video', (data) => {
            console.log('data', data)
        })

        socket.on('pong', () => {
            setLastPong(new Date().toISOString())
        })

        return () => {
            socket.off('connect')
            socket.off('disconnect')
            socket.off('pong')
        }
    }, [])

    const sendPing = () => {
        socket.emit('ping')
    }

    const handleScalingChange = (event) => {
        const value = 1 + (event.target.value / 100)
        setScaling(value)
        onScalingChange(value)
    }

    const renderScalingPopup = () => (
        <div className='scaling-popup'>
            <label htmlFor="scaling">Scaling</label>
            <input type="range" name="scaling" id="scaling" min={0} max={500} value={(scaling - 1) * 100} onChange={handleScalingChange} />
            <button onClick={() => setIsShowScalingPopup(false)}>Close</button>
        </div>
    )

    return (
        <div>
            <p>Connected: { '' + isConnected }</p>
            <p>Last pong: { lastPong || '-' }</p>

            <button onClick={ sendPing }>Send ping</button>

            <div>
                <label htmlFor="scaling">Scaling: {scaling}</label>
                <button id="scaling" onClick={() => setIsShowScalingPopup(true)}>Change image scaling</button>
            </div>

            <div>
                <label htmlFor="binFile">BIN File</label>
                <select name="binFile" id="binFile">
                    <option value="test-a.6502.bin">test-a.6502</option>
                    <option value="test-b.6502.bin">test-b.6502</option>
                    <option value="test-c.6502.bin">test-c.6502</option>
                </select>
                <button>Refresh List</button>
                <button>Reload Program</button>
                <button>Blow Into Cartridge</button>

            </div>
            {isShowScalingPopup && renderScalingPopup()}
        </div>
    )
}

export default Control
