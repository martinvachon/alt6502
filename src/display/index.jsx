import React, { useEffect, useRef } from 'react'
import io from 'socket.io-client'

const socket = io('http://localhost:3000')

// NES resolution
const WIDTH = 256
const HEIGHT = 240

/**
 *
 * https://socket.io/how-to/use-with-react-hooks
 */
function Display ({ scaling }) {
    const canvasRef = useRef(null)
    let videoAddresses = []

    const renderVideoMemory = (context) => {
        let address = -1

        for (let w = 0; w < WIDTH; w++) {
            for (let h = 0; h < HEIGHT; h++) {
                address++
                context.fillStyle = '#' + videoAddresses[address]
                context.fillRect(w, h, 1, 1)
            }
        }
    }

    useEffect(() => {
        const canvas = canvasRef.current
        const context = canvas.getContext('2d')
        context.scale(scaling, scaling)

        renderVideoMemory(context)

        socket.on('init-video', (videos) => {
            videoAddresses = videos
            renderVideoMemory(context)
        })

        socket.on('update-video', (updates) => {
            updates.forEach((update) => {
                const address = update[0]

                videoAddresses[address] = update[1]

                context.fillStyle = '#' + update[1]

                const x = address % WIDTH
                const y = Math.floor(address / WIDTH)

                context.fillRect(x, y, 1, 1)

                console.log(updates, '#' + update[1], x, y)
            })
        })

        socket.on('display', (data) => {
            context.clearRect(0, 0, context.canvas.width, context.canvas.height)
            context.fillStyle = '#000000'

            context.font = '16px serif'
            context.fillText('Hello world', 10, 50)
            context.fillText(new Date().toISOString(), 10, 75)
            context.fillText(data, 10, 100)

            context.fillStyle = '#FF0000'

            context.fillRect(0, 120, context.canvas.width, 10)
            context.fillRect(0, 150, 256, 1)
            context.fillRect(200, 200, 1, 1)
        })

        return () => {
            socket.off('display')
        }
    }, [scaling])

    return (
        <div>
            <canvas width={WIDTH * scaling} height={HEIGHT * scaling} ref={canvasRef} />
        </div>
    )
}

export default Display
