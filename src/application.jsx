import React from 'react'

import Display from './display'
import Control from './control'

import './style/alt6502.css'

class Application extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            scaling: 1
        }
    }

    handleScalingChange = (scaling) => {
        this.setState({ scaling })
    }

    render () {
        return (
            <div className='alt6502'>
                <header>
                    <h1>ALT6502</h1>
                </header>
                <main>
                    <Display scaling={this.state.scaling} />
                </main>
                <aside>
                    <Control onScalingChange={this.handleScalingChange}/>
                </aside>
                <footer>
                    console
                </footer>
            </div>
        )
    }
}

export default Application
