import React from 'react'
import ReactDOM from 'react-dom/client'

import Application from './application'

const root = ReactDOM.createRoot(document.getElementById('app'))

root.render(
    <React.StrictMode>
        <Application/>
    </React.StrictMode>
)
