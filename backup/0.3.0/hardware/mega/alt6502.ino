/**
 *
 * References:
 *  https://www.instructables.com/Arduino-Timer-Interrupts/
 **/

// Connected to getter, timer1 produce a pulse to CLOCK_OUT and
// the interrupt on CLOCK_IN execute the monitoring.
// W65C02S: PHI2 (37)
#define CLOCK_IN 3  // interrupt
#define CLOCK_OUT 11

// W65C02S: RWB (34)
#define READ_WRITE 10

// W65C02S: A15 (25) - A0 (9)
const char ADDRESS_PINS[] = {25, 23, 22, 24, 26, 28, 30, 32, 34, 36, 38, 40, 42, 44, 46, 48};

// W65C02S: D7 (26) - D0 (33)
const char DATA_PINS[] = {41, 39, 37, 35, 33, 31, 29, 27};

// AD03 - AD00
const char RANGE_PINS[] = {4, 5, 6, 7};

byte program[100];

// 2 0 40 10 40 10 40 1F 40
// byte A9 FF 8D 0 40 8D 2 40 A9 55 EA 2A 8D 0 40 4C B 80
// const byte program1[] = {0x2, 0x0, 0x40, 0x10, 0x40, 0x10, 0x40, 0x1F, 0x40, 0xA9, 0x55, 0xEA, 0x2A, 0x8D, 0x10, 0x10, 0x4C, 0x00, 0x10};

void setPinsMode(const char pins[], uint8_t count, uint8_t mode) {
    for (int n = 0; n < count; n += 1) {
        pinMode(pins[n], mode);
    }
}

void setDataPins(int opcode) {
    setPinsMode(DATA_PINS, 8, OUTPUT);

    boolean bits[] = {0, 0, 0, 0, 0, 0, 0, 0};  // TODO
    for (int i = 7; i >= 0; i--) {
        bits[i] = bitRead(opcode, i);
        digitalWrite(DATA_PINS[7 - i], (bits[i] == 0 ? LOW : HIGH));
    }
}

void setAddressDecodePins(short range) {
    setPinsMode(RANGE_PINS, 4, OUTPUT);

    // boolean bits[] = {0, 0, 0, 0};// TODO
    for (int i = 3; i >= 0; i--) {
        // bits[i] = bitRead(range, i);
        digitalWrite(RANGE_PINS[3 - i], (bitRead(range, i) == 0 ? LOW : HIGH));
    }
}

void loadProgram() {
    Serial.readBytes(program, 100);

    for (uint8_t c = 0; c < 100; c++) {
        Serial.print(program[c], HEX);
        Serial.print(' ');
    }

    Serial.println();
}

boolean stepFlag = false;
unsigned int stepCount = 1;
unsigned int stepIndex = 0;

void stopProgram() {
    stepFlag = false;

    // stop interrupts
    cli();

    // turn off interrupt on timer 1 Compare A match
    TIMSK1 &= ~bit(OCIE1A);

    // allow interrupts
    sei();
}

void runProgram(short range, unsigned int speed) {
    stepFlag = false;

    // stop interrupts
    cli();
    // set up Timer 1
    // TCCR1A = bit(COM1A0);  // toggle OC1A on Compare Match
    TCCR1A = 0;
    TCCR1B = 0;

    TCNT1 = 0;            // <---------------- clear the timer
    TIFR1 |= bit(OCF1A);  // clear Output Compare A Match Flag

    // turn on CTC mode
    TCCR1B |= (1 << WGM12);

    // Set CS12 and CS10 bits for 1024 prescaler
    // TCCR1B |= (1 << CS12) | (1 << CS10);

    /**
     * The prescaler determine the timer speed:
     *      (timer speed(Hz)) = (Arduino clock speed(16MHz)) / prescaler
     *      timer speed = 16000000 / 1
     **/
    switch (range) {
        case 0:
            // 1024 CS12 + CS10
            TCCR1B |= (1 << CS12) | (1 << CS10);
            break;
        case 1:
            // 256  CS12
            TCCR1B |= (1 << CS12);

            break;
        case 2:
            // 64   CS11 + CS10
            TCCR1B |= (1 << CS11) | (1 << CS10);
            break;
        case 3:
            // 8    CS11
            TCCR1B |= (1 << CS11);
            break;
        case 4:
            // 1    CS10
            TCCR1B |= (1 << CS10);
            break;

        default:
            break;
    }

    // interrupt frequency (Hz) = (Arduino clock speed 16,000,000Hz) / (prescaler * (compare match register + 1))
    // compare match register = [ 16,000,000Hz / (prescaler * desired interrupt frequency) ] - 1
    // set compare match register for 1hz increments
    OCR1A = speed;

    // Set CS01 and CS00 bits for 64 prescaler
    // TCCR1B |= (1 << CS01) | (1 << CS00);
    // TCCR1B |= (1 << CS12);
    // set compare match register for 2khz increments
    // OCR1A = 124;

    TIMSK1 = 0;
    // enable interrupt on Timer 1 Compare A match
    TIMSK1 = (1 << OCIE1A);

    // allow interrupts
    sei();

    Serial.print("Run program: ");
    Serial.print(range);
    Serial.print(" ");
    Serial.println(speed);
}

ISR(TIMER1_COMPA_vect) { executeClock(); }

void stepProgram(unsigned int count) {
    Serial.print("Step program: ");
    Serial.println(count);
    stopProgram();

    stepCount = count;
    stepIndex = 0;
    stepFlag = true;
}

void executeClock() {
    digitalWrite(CLOCK_OUT, LOW);

    delay(1);

    digitalWrite(CLOCK_OUT, HIGH);
    // delayMicroseconds(1);
    delay(1);
}

/**
 *
 **/
short getAddressRange(word address) {
    short range = 0;

    uint8_t rangeCounter = 0;

    while ((rangeCounter < program[0]) && (range == 0)) {
        // Each range is defined by start and end address
        const uint8_t offset = rangeCounter * 4;

        word startAddress = program[offset + 2];
        startAddress = (startAddress << 8) + program[offset + 1];

        word endAddress = program[offset + 4];
        endAddress = (endAddress << 8) + program[offset + 3];

        if (address >= startAddress && address <= endAddress) {
            range = rangeCounter + 1;
        }

        rangeCounter++;
    }

    return range;
}

void onClock() {
    unsigned int address = 0;
    unsigned int data = 0;
    unsigned int ad = 0;

    for (int n = 0; n < 16; n += 1) {
        int bit = digitalRead(ADDRESS_PINS[n]) ? 1 : 0;
        // Serial.print(bit);
        address = (address << 1) + bit;
    }

    ad = getAddressRange(address);
    setAddressDecodePins(ad);

    char rw = digitalRead(READ_WRITE) ? 'R' : 'W';
    char irq = 0;  // TODO

    if (rw == 'W') {
        setPinsMode(DATA_PINS, 8, INPUT);
        for (int n = 0; n < 8; n += 1) {
            int bit = digitalRead(DATA_PINS[n]) ? 1 : 0;
            data = (data << 1) + bit;
        }
    } else {
        // Reset vector address
        if (address == 0xFFFC) {
            data = (program[0] * 4) + 1;
            setDataPins(data);
        }
        // Reset vector address
        else if (address == 0xFFFD) {
            data = B10000000;
            setDataPins(data);
        }
        // ROM emulation addresses
        else if ((address >= 0x8000) && (address < 0xFFFA)) {
            int offset = address - 0x8000;
            data = program[offset];
            setDataPins(data);
        } else {
            setPinsMode(DATA_PINS, 8, INPUT);
            for (int n = 0; n < 8; n += 1) {
                int bit = digitalRead(DATA_PINS[n]) ? 1 : 0;
                data = (data << 1) + bit;
            }
        }
    }

    if (stepFlag) {
        // Address  Data    AD  R/W IRQ
        // FFFF     FF      F   0   0
        char output[15];
        sprintf(output, "%04X %02X %01X %c %d", address, data, ad, rw, irq);
        Serial.println(output);
    }
}

void setup() {
    setPinsMode(ADDRESS_PINS, 16, INPUT);
    pinMode(CLOCK_IN, INPUT);
    pinMode(CLOCK_OUT, OUTPUT);
    pinMode(READ_WRITE, INPUT);

    attachInterrupt(digitalPinToInterrupt(CLOCK_IN), onClock, RISING);

    Serial.begin(115200);
    delay(1000);

    Serial.println("setup");
}

const char LOAD_COMMAND = 'l';
const char STEP_COMMAND = 's';
const char RUN_COMMAND = 'r';
const char STOP_COMMAND = 'p';

char command = 0;

unsigned int incomingByte;
unsigned int parameters[2] = {0, 0};
byte parameterCounter = 0;

void processCommand() {
    while (Serial.available()) {
        // Everything start with a command as first byte
        if (command == 0) {
            incomingByte = Serial.read();
            // If the first byte is a valid command
            if (incomingByte == LOAD_COMMAND || incomingByte == STEP_COMMAND || incomingByte == RUN_COMMAND || incomingByte == STOP_COMMAND) {
                command = incomingByte;

                // Special case for load command because the parameter is the program read by the method loadProgram()
                if (command == LOAD_COMMAND) {
                    loadProgram();
                    // No need to continue further
                    command = 0;
                }
            }
        } else if (command == STEP_COMMAND || command == RUN_COMMAND || command == STOP_COMMAND) {
            // Read parameter value of previous command
            incomingByte = Serial.read();
            if (incomingByte >= '0' && incomingByte <= '9') {
                parameters[parameterCounter] = (parameters[parameterCounter] * 10) + (incomingByte - '0');
            }
            // Parameter separator
            else if (incomingByte == '|') {
                parameterCounter++;
            }
            // End of serial stream, execute command
            else if (incomingByte == 10) {
                if (command == STEP_COMMAND) {
                    stepProgram(parameters[0]);
                } else if (command == RUN_COMMAND) {
                    runProgram(parameters[0], parameters[1]);
                } else if (command == STOP_COMMAND) {
                    stopProgram();
                }

                parameterCounter = 0;
                parameters[0] = 0;
                parameters[1] = 0;
                command = 0;
            }
        }
    }
}

void loop() {
    processCommand();

    if (stepFlag && stepIndex < stepCount) {
        executeClock();
        delay(50);
        stepIndex++;

        // Reset the step flag at the end
        if (stepIndex >= stepCount) {
            stepFlag = false;
        }
    }
}