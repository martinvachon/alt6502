#include <Arduino.h>

#define CLOCK_IN_PIN 2  // interrupt
#define RW_PIN 3

// CA15, ... CA0
const char ADDRESS_PINS[] = {27, 26, 25, 24, 20, 21, 22, 23, 14, 15, 16, 17, 18, 19, 1, 0};

// CD7, ... CD0
const char DATA_PINS[] = {5, 6, 7, 8, 9, 10, 11, 12};

const byte program[] = {0x2, 0x0, 0x40, 0x10, 0x40, 0x10, 0x40, 0x1F, 0x40, 0xA9, 0x55, 0xEA, 0x2A, 0x8D, 0x10, 0x10, 0x4C, 0x00, 0x10};

void setPinsMode(const char pins[], uint8_t count, uint8_t mode) {
    for (int n = 0; n < count; n += 1) {
        pinMode(pins[n], mode);
    }
}

void setDataPins(int opcode) {
    setPinsMode(DATA_PINS, 8, OUTPUT);

    boolean bits[] = {0, 0, 0, 0, 0, 0, 0, 0};  // TODO
    for (int i = 7; i >= 0; i--) {
        bits[i] = bitRead(opcode, i);
        digitalWrite(DATA_PINS[7 - i], (bits[i] == 0 ? LOW : HIGH));
    }
}

void onClock() {
    unsigned int address = 0;
    unsigned int data = 0;
    unsigned int ad = 0;

    char irq = 0;  // TODO
    char rw = digitalRead(RW_PIN) ? 'R' : 'W';

    for (int n = 0; n < 16; n += 1) {
        int bit = digitalRead(ADDRESS_PINS[n]) ? 1 : 0;
        // Serial.print(bit);
        address = (address << 1) + bit;
        // Serial.print(bit);
    }

    // Serial.println(' ');

    if (rw == 'W') {
        setPinsMode(DATA_PINS, 8, INPUT);
        for (int n = 0; n < 8; n += 1) {
            int bit = digitalRead(DATA_PINS[n]) ? 1 : 0;
            data = (data << 1) + bit;
        }
    } else {
        // Reset vector address
        if (address == 0xFFFC) {
            data = (program[0] * 4) + 1;
            setDataPins(data);
        }
        // Reset vector address
        else if (address == 0xFFFD) {
            data = B10000000;
            setDataPins(data);
        }
        // ROM emulation addresses
        else if ((address >= 0x8000) && (address < 0xFFFA)) {
            int offset = address - 0x8000;
            data = program[offset];
            setDataPins(data);
        } else {
            setPinsMode(DATA_PINS, 8, INPUT);
            for (int n = 0; n < 8; n += 1) {
                int bit = digitalRead(DATA_PINS[n]) ? 1 : 0;
                data = (data << 1) + bit;
            }
        }
    }

    // Address  Data    AD  R/W IRQ
    // FFFF     FF      F   0   0
    char output[15];
    sprintf(output, "%04X %02X %01X %c %d", address, data, ad, rw, irq);
    Serial.println(output);
}

void setup() {
    setPinsMode(ADDRESS_PINS, 16, INPUT);
    pinMode(CLOCK_IN_PIN, INPUT);

    attachInterrupt(digitalPinToInterrupt(CLOCK_IN_PIN), onClock, RISING);  // FALLING
    Serial.begin(115200);
}

void loop() {
    // Serial.println("Hello World");
    //  delay(1000);
}