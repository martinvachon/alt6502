                    .org    $8000                               ; Predefined memory mapping

; Addresses decoder configuration
;-------------------------------------------------------------------------------------------

VIA_1               .equ    $4000
LCD                 .equ    VIA_1 + 16

                    .byte   (config_end - config_start) / 4     ; Addresses range count (Use 4 bits only of a byte)

config_start:
                    .word   VIA_1, LCD                          ; range 0000    4000 to 4010
                    .word   LCD, LCD + 15                       ; range 0001    4010 to 401F
config_end:

; Program code start
;-------------------------------------------------------------------------------------------

                    LDA     #$ff                                ; Configure 6522 for write mode on port B
                    STA     $4002                               ; Store configuration in 6522

program_start       LDA     #$55                                ; Binary value for led effect: 1010101
                    STA     $4000                               ; Write value in 6522 port B

                    ROL                                         ; Rotate one bit left
                    STA     $4000

                    JMP     program_start


