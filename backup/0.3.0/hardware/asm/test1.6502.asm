; Program counter start at 8000

.org    $8000

                LDA     #$ff
                STA     $4000
                STA     $4002

                LDA     #$55    ; Binary value for led effect: 1010101
                NOP             ;
                ROL

                STA     $4000
                JMP     $800b
