const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
// const Ready = require('@serialport/parser-ready')

class SerialPortService {
    getUsbPort () {
        return this.serialPort && this.serialPort.isOpen ? this.serialPort.path : ''
    }

    disconnect (successCallback, errorCallback) {
        if (this.serialPort) {
            this.serialPort.close(function (error) {
                if (error) {
                    errorCallback(error)
                } else {
                    successCallback('Success')
                }
            })
        } else {
            successCallback('No active connection')
        }
    }

    connect (usbPort, baudRate, socket, successCallback, errorCallback) {
        this.serialPort = new SerialPort(usbPort, {
            baudRate: baudRate,
            autoOpen: false
        })

        this.serialPort.on('error', function (data) {
            console.error('Error: ' + data)
        })

        const parser = this.serialPort.pipe(new Readline({ delimiter: '\n' }))
        parser.on('data', (data) => {
            const value = data.toString('utf-8')
            socket.emit('logging', {
                data: value
            })
            console.log('DATA', value)
        })

        this.serialPort.open(function (error) {
            if (error) {
                errorCallback(error)
            } else {
                successCallback(usbPort)
            }
        })
    }

    write (message, successCallback, errorCallback) {
        this.serialPort.write(message, (error) => {
            if (error) {
                errorCallback(error)
            } else {
                successCallback()
            }
        })
    }

    getPorts (successCallback, errorCallback) {
        SerialPort.list().then(
            ports => successCallback(ports),
            error => errorCallback(error)
        )
    }
}

module.exports = {
    SerialPortService
}
