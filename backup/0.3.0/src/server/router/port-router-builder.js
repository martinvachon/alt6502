const express = require('express')
const { StatusCodes } = require('http-status-codes')

function portRouterBuilder (serialPortService, socketServer) {
    const router = express.Router()

    async function getLoggingSocket () {
        const sockets = await socketServer.fetchSockets() // .in('logging')
        // console.log('SOCKETS', sockets)
        return sockets
    }

    router.get('/', function (request, response) {
        serialPortService.getPorts(
            (ports) => response.status(StatusCodes.OK).send(ports),
            (error) => console.error(error) // TODO
        )
    })

    router.get('/status', function (request, response) {
        response.status(StatusCodes.OK).send({
            status: serialPortService.getUsbPort()
        })
    })

    router.get('/disconnect', function (request, response) {
        const successCallback = (message) => response.status(StatusCodes.OK).send({ result: message })
        const errorCallback = (error) => response.status(StatusCodes.FORBIDDEN).send({ result: error.message })
        serialPortService.disconnect(successCallback, errorCallback)
    })

    router.post('/', async function (request, response) {
        console.log(request.body)
        const { port, speed } = request.body

        const successCallback = (port) => response.status(StatusCodes.OK).send({ result: 'Success', port: port })
        const errorCallback = (error) => response.status(StatusCodes.FORBIDDEN).send({ result: error.message, port: port })

        const socket = await getLoggingSocket() // TODO

        serialPortService.connect(port, speed, socket[0], successCallback, errorCallback)
    })

    return router
}

module.exports = {
    portRouterBuilder
}
