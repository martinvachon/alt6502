const express = require('express')
const { StatusCodes } = require('http-status-codes')
const path = require('path')
const fs = require('fs')

function binRouterBuilder () {
    const router = express.Router()

    router.get('/', function (request, response) {
        const isFile = fileName => {
            return fs.lstatSync(fileName).isFile() && fileName.endsWith('.bin')
        }

        // TODO
        const folderPath = 'C:/dev/vsc-workspace/alt6502/hardware/asm'

        const binFiles = fs.readdirSync(folderPath).map(fileName => {
            return path.join(folderPath, fileName)
        })
            .filter(isFile)

        response.status(StatusCodes.OK).send(binFiles)
    })

    return router
}

module.exports = {
    binRouterBuilder
}
