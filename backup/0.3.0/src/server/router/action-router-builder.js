const express = require('express')
const fs = require('fs')
const { StatusCodes } = require('http-status-codes')

function actionRouterBuilder (serialPortService) {
    const router = express.Router()

    router.get('/stop', function (request, response) {
        const message = 'p\n'
        serialPortService.write(message, function (error) {
            if (error) {
                console.error('Error writing on serial port: ', error.message) // TODO
            }
            console.log('Sending message:', message)
            response.status(StatusCodes.OK).send({ result: 'Success' })
        })
    })

    router.post('/step', function (request, response) {
        const message = 's' + request.body.stepCount + '\n'
        serialPortService.write(message, function (error) {
            if (error) {
                console.error('Error writing on serial port: ', error.message) // TODO
            }
            console.log('Sending message:', message)
            response.status(StatusCodes.OK).send({ result: 'Success' })
        })
    })

    router.post('/run', function (request, response) {
        console.log('BODY', request.body)
        const message = 'r' + request.body.speedRange + '|' + request.body.runSpeed + '\n'
        serialPortService.write(message, function (error) {
            if (error) {
                console.error('Error writing on serial port: ', error.message) // TODO
            }
            console.log('Sending message:', message)
            response.status(StatusCodes.OK).send({ result: 'Success' })
        })
    })

    router.post('/load', function (request, response) {
        const binFile = request.body.bin
        try {
            const command = Buffer.from('l') // todo
            const bin = fs.readFileSync(binFile)
            const message = Buffer.concat([command, bin], command.length + bin.length)

            serialPortService.write(message, function (error) {
                if (error) {
                    console.error('Error writing on serial port: ', error.message) // TODO
                }
                console.log('Sending program:', message) // TODO
                response.status(StatusCodes.OK).send({ result: 'Success' })
            })
        } catch (error) {
            console.error('File not found:', binFile) // TODO
        }
    })

    return router
}

module.exports = {
    actionRouterBuilder
}
