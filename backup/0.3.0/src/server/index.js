'use strict'

const express = require('express')
const { Server } = require('socket.io')
const http = require('http')
const path = require('path')
const open = require('open')

const { SerialPortService } = require('./service/serial-port-service')

const { actionRouterBuilder } = require('./router/action-router-builder')
const { binRouterBuilder } = require('./router/bin-router-builder')
const { portRouterBuilder } = require('./router/port-router-builder')

function startServer (folder) {
    const PORT = 8080

    const serialPortService = new SerialPortService()
    const server = http.createServer(express)
    const socketServer = new Server(server, {
        cors: {
            origin: 'http://localhost:8081',
            methods: ['*']
        }
    })

    const app = express()
    app.set('json spaces', 4)
    app.use(express.static(path.join(__dirname, '..', '..', 'dist', 'client')))

    app.use(express.json())
    app.use(express.urlencoded({ extended: true }))

    // CORS for development
    // https://enable-cors.org/server_expressjs.html
    app.use(function (request, response, next) {
        response.header('Access-Control-Allow-Origin', '*')
        response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
        response.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE, OPTIONS')
        response.header('Access-Control-Allow-Credentials', 'false')
        next()
    })

    app.use('/ports', portRouterBuilder(serialPortService, socketServer))
    app.use('/bins', binRouterBuilder())
    app.use('/actions', actionRouterBuilder(serialPortService))

    /* async function getSocket () {
    const sockets = await socketServer.in('logging').fetchSockets()
    console.log(sockets)
    return sockets
} */

    // let socket
    socketServer.on('connection', (defaultSocket) => {
        console.log('socket connected', defaultSocket.id)

        // socket = defaultSocket
    })
    server.listen(3000, () => {
        console.log('Socket listening on *:3000')
    })

    app.listen(PORT, function () {
        console.log('Server listening on: http://localhost:%s', PORT)

        const installationFolder = path.join(__dirname, '..', '..')
        console.log('installationFolder', installationFolder)
        open('http://localhost:8080/')
    })
}

module.exports = {
    startServer
}
