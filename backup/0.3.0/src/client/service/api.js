
export class API {
    constructor (baseUrl) {
        this.baseUrl = baseUrl
    }

    post (endpoint, body, callback) {
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(body)
        }
        fetch(this.baseUrl + endpoint, fetchConfig)
            .then(response => response.json())
            .then(response => callback(response))
    }

    get (endpoint, callback) {
        fetch(this.baseUrl + endpoint, { method: 'GET' })
            .then(response => response.json())
            .then(response => callback(response))
    }

    getPortStatus (callback) {
        this.get('/ports/status', callback)
    }

    getPorts (callback) {
        this.get('/ports', callback)
    }

    getBins (callback) {
        this.get('/bins', callback)
    }

    disconnectPort (callback) {
        this.get('/ports/disconnect', callback)
    }

    connectPort (port, speed, callback) {
        this.post('/ports', { port, speed }, callback)
    }

    executeStep (stepCount, callback) {
        this.post('/actions/step', { stepCount }, callback)
    }

    executeRun (speedRange, runSpeed, callback) {
        this.post('/actions/run', { speedRange, runSpeed }, callback)
    }

    executeStop (callback) {
        this.get('/actions/stop', callback)
    }

    executeLoad (body, callback) {
        this.post('/actions/load', body, callback)
    }
}
