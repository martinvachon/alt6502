
const SIZE = 65535

const RAM_TYPE = 0
const ROM_TYPE = 1
const USER_TYPE = 2

const USER_START = 0x0200
const USER_END = 0xFFF9

const RAM_PAGE_0_START = 0x0000
const RAM_PAGE_0_END = 0x00FF

const RAM_PAGE_1_START = 0x0100
const RAM_PAGE_1_END = 0x01FF

const ROM_RMI_START = 0xFFFA
const ROM_RMI_END = 0xFFFB

const ROM_RESET_START = 0xFFFC
const ROM_RESET_END = 0xFFFD

const ROM_IRQ_START = 0xFFFE
const ROM_IRQ_END = 0xFFFF

function initializeMemoryRange (type, start, end, description) {
    const addresses = []

    for (let a = start; a <= end; a++) {
        addresses[a] = '--'
    }

    return {
        type: type,
        start: start,
        end: end,
        description: description,
        addresses: addresses
    }
}

function initializeMemoryMap () {
    const page0 = initializeMemoryRange(
        RAM_TYPE,
        RAM_PAGE_0_START,
        RAM_PAGE_0_END,
        '0 Or zero page, ZP: Pointer addresses for pre-indexed and post-indexed indirect addressing modes.')

    const page1 = initializeMemoryRange(
        RAM_TYPE,
        RAM_PAGE_1_START,
        RAM_PAGE_1_END,
        'Hardware stack, use to find the way back from subroutines and interrupts')

    const user = initializeMemoryRange(
        USER_TYPE,
        USER_START,
        USER_START + 16, // USER_END,
        'User')

    const rmi = initializeMemoryRange(
        ROM_TYPE,
        ROM_RMI_START,
        ROM_RMI_END,
        'NMI: Non-maskable interrupts.'
    )

    const reset = initializeMemoryRange(
        ROM_TYPE,
        ROM_RESET_START,
        ROM_RESET_END,
        'Reset: The program  counter  is  loaded  with  the  reset  vector  from  locations  FFFC  (low  byte)  and  FFFD  (high  byte)')

    const irq = initializeMemoryRange(
        ROM_TYPE,
        ROM_IRQ_START,
        ROM_IRQ_END,
        'IRQ: Maskable Interrupt request.')

    return { page0, page1, user, rmi, reset, irq }
}

export {
    initializeMemoryMap
}
