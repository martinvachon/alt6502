import React from 'react'
import ReactDOM from 'react-dom'

import ALT6502 from './alt6502'

import './style/alt6502.css'

ReactDOM.render(
    <ALT6502 />,
    document.getElementById('app')
)
