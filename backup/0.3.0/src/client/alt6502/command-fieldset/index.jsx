import React from 'react'

const CommandFieldset = ({ title, className, commands, children }) => (
    <fieldset className={className}>
        <legend>{title}</legend>
        <div className="field">
            {children}
        </div>
        <div className="command">
            {commands.map((command, index) => <button key={index} onClick={command.onClick} disabled={command.disabled}>{command.label}</button>)}
        </div>
    </fieldset>
)

export default CommandFieldset
