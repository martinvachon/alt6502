import React from 'react'

function renderRange (range) {
    return range.addresses.map((data, address) =>
        <tr key={address} className={'type-' + range.type}>
            <td>{address.toString(16).toUpperCase().padStart(4, '0')}</td>
            <td>{data.toString(16).toUpperCase().padStart(2, '0')}</td>
            <td>{range.type}</td>
        </tr>
    )
}

export const MemoryMap = ({ memoryMap }) => (
    <table className="memory-table">
        <thead>
            <tr>
                <th>Address</th>
                <th>Data</th>
                <th>Type</th>
            </tr>
        </thead>
        <tbody>
            {renderRange(memoryMap.page0)}
            {renderRange(memoryMap.page1)}
            {renderRange(memoryMap.user)}
            {renderRange(memoryMap.rmi)}
            {renderRange(memoryMap.reset)}
            {renderRange(memoryMap.irq)}
        </tbody>
    </table>
)
