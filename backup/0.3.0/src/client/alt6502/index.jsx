import React, { Component } from 'react'
import { io } from 'socket.io-client'

import CommandFieldset from './command-fieldset'
import { API } from '../service/api'

import switchOpenIcon from '../style/image/switch-open.svg'
import switchCloseIcon from '../style/image/switch-close.svg'
import { initializeMemoryMap } from '../service/memory'
import { MemoryMap } from './memory-map'

const SPEED_OPTIONS = [300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 28800, 31250, 38400, 57600, 115200, 230400, 250000, 500000, 1000000, 2000000]
const STEP_COUNT_OPTIONS = [1, 2, 3, 4, 5, 10, 20, 30, 50, 100, 200]
const RUN_SPEED_RANGE_OPTIONS = [
    {
        min: 124,
        max: 61400,
        prescaler: 1024,
        label: '0.25 Hz to 125 Hz'
    },
    {
        min: 60,
        max: 499,
        prescaler: 256,
        label: '125 Hz to 1 KHz'
    },
    {
        min: 1,
        max: 249,
        prescaler: 64,
        label: '1 Khz to 125 KHz'
    },
    {
        min: 1,
        max: 15,
        prescaler: 8,
        label: '125 KHz to 1 MHz'
    },
    {
        min: 1,
        max: 15,
        prescaler: 1,
        label: '1 MHz to 8 MHz'
    }
]

class ALT6502 extends Component {
    constructor (props) {
        super(props)

        this.loggingTBody = React.createRef()

        this.api = new API('http://localhost:8080')

        this.state = {
            ports: [],
            bins: [],
            port: '',
            bin: '',
            speed: 115200,
            connected: false,
            stepCount: 1,
            runSpeed: 1,
            speedRange: 0,
            loggings: [],
            memoryMap: initializeMemoryMap()
        }

        const socket = io('http://localhost:3000')

        socket.on('connect', () => {
            console.log('connect', socket.id)

            socket.on('logging', (data) => {
                console.log('socket data:', data)
                this.setState({
                    loggings: [...this.state.loggings, data]
                })
            })
        })
        socket.on('disconnect', () => {
            console.log('disconnect', socket.id)
        })
    }

    componentDidMount () {
        this.refreshConnectionStatus()
        this.scrollToBottom()
    }

    componentDidUpdate () {
        this.scrollToBottom()
    }

    refreshConnectionStatus = () => {
        this.api.getPortStatus((response) => this.setState({ port: response.status, connected: response.status !== '' }))
        this.api.getPorts((ports) => this.setState({ ports: ports }))
    }

    onSpeedRangeChange = (event) => {
        const index = parseInt(event.target.value, 10)
        this.setState({
            speedRange: index,
            runSpeed: RUN_SPEED_RANGE_OPTIONS[index].max
        })
    }

    onRunSpeedChange = (event) => {
        this.setState({
            runSpeed: parseInt(event.target.value, 10)
        })
    }

    onBinSelectChange = (event) => {
        const value = event.target.value
        this.setState({
            bin: value
        })
    }

    renderLoggingTr = (loggings) => {
        return loggings.map((logging, index) => {
            let tr

            const datas = logging.data.split(' ')
            if (datas.length === 5) {
                const address = parseInt(datas[0], 16)
                const data = parseInt(datas[1], 16)
                const ad = parseInt(datas[2], 16)
                tr = (
                    <tr key={index} data-address={datas[0]}>
                        <td className="address-bin-column">{address.toString(2).padStart(16, 0)}</td>
                        <td className="data-bin-column">{data.toString(2).padStart(8, 0)}</td>
                        <td className="range-bin-column">{ad.toString(2).padStart(4, 0)}</td>
                        <td className="address-hex-column">{datas[0]}</td>
                        <td className="data-hex-column">{datas[1]}</td>
                        <td className="range-hex-column">{datas[2]}</td>
                        <td className="rw-column">{datas[3]}</td>
                        <td className="irq-column">{datas[4]}</td>
                    </tr>
                )
            } else {
                tr = (
                    <tr key={index}>
                        <td colSpan={5}>{logging.data}</td>
                    </tr>
                )
            }

            return tr
        })
    }

    scrollToBottom = () => {
        this.loggingTBody.current.scrollBy(0, 100)
    }

    calculProcessorSpeed = () => {
        // interrupt frequency (Hz) = (Arduino clock speed 16,000,000Hz) / (prescaler * (compare match register + 1))

        const prescaler = RUN_SPEED_RANGE_OPTIONS[this.state.speedRange].prescaler

        const frequency = 16000000 / (prescaler * (this.state.runSpeed + 1))
        return frequency.toFixed(2).toLocaleString()
    }

    renderMemoryRows = () => {
        console.log(this.state.memoryMap)
        return ''
    }

    render () {
        console.log(this.state.memoryMap)

        const CONNECTION_FORM_COMMANDS = [
            {
                label: 'Refresh',
                disabled: false,
                onClick: this.refreshConnectionStatus
            },
            {
                label: 'Connect',
                disabled: this.state.port === '' || this.state.connected,
                onClick: () => this.api.connectPort(this.state.port, this.state.speed, () => this.setState({ connected: true }))
            },
            {
                label: 'Disconnect',
                disabled: !this.state.connected,
                onClick: () => this.api.disconnectPort(() => this.setState({ connected: false }))
            }
        ]

        const STEP_FORM_COMMANDS = [
            {
                label: 'Step',
                disabled: !this.state.connected,
                onClick: () => this.api.executeStep(this.state.stepCount, (result) => console.log(result))
            },
            {
                label: 'Stop',
                disabled: !this.state.connected,
                onClick: () => this.api.executeStop((result) => console.log(result))
            }
        ]

        const RUN_FORM_COMMANDS = [
            {
                label: 'Run',
                disabled: !this.state.connected,
                onClick: () => this.api.executeRun(this.state.speedRange, this.state.runSpeed, (result) => console.log(result))
            },
            {
                label: 'Stop',
                disabled: !this.state.connected,
                onClick: () => this.api.executeStop((result) => console.log(result))
            }
        ]

        const LOAD_FORM_COMMANDS = [
            {
                label: 'Refresh',
                disabled: false,
                onClick: () => this.api.getBins((result) => this.setState({ bins: result }))
            },
            {
                label: 'Load',
                disabled: this.state.bin === '' || !this.state.connected,
                onClick: () => this.api.executeLoad({ bin: this.state.bin }, (result) => console.log(result))
            }
        ]

        return (
            <div id="alt6502">
                <header>
                    <h1>
                        <strong>A</strong>ssembly <strong>L</strong>anguage <strong>T</strong>rainer for <strong>6502</strong>.
                    </h1>
                </header>
                <main>
                    <section className="memory">
                        <div className="memory-table-container">
                            <MemoryMap memoryMap={this.state.memoryMap}/>
                        </div>
                    </section>
                    <section className="log">
                        <div className="logging-table">
                            <table>
                                <thead>
                                    <tr>
                                        <th className="address-bin-column">Address</th>
                                        <th className="data-bin-column">Data</th>
                                        <th className="range-bin-column">Range</th>
                                        <th className="address-hex-column">A</th>
                                        <th className="data-hex-column">D</th>
                                        <th className="range-hex-column">R</th>
                                        <th className="rw-column">RW</th>
                                        <th className="irq-column">IRQ</th>
                                    </tr>
                                </thead>
                                <tbody ref={this.loggingTBody}>
                                    {this.renderLoggingTr(this.state.loggings)}
                                </tbody>
                            </table>
                        </div>
                    </section>
                    <section className="form">
                        <fieldset className="connection-form">
                            <legend>Connection <img src={this.state.connected ? switchCloseIcon : switchOpenIcon} alt="Connection" /></legend>
                            <div className="field">
                                <label htmlFor="portSelect">Port</label>
                                <select id="portSelect" onChange={event => this.setState({ port: event.target.value })} value={this.state.port} disabled={this.state.connected}>
                                    <option value="">Select port</option>
                                    {this.state.ports.map((port, index) => <option key={index} value={port.path}>{port.path} {port.manufacturer}</option>)}
                                </select>
                                <label htmlFor="speedSelect">Speed</label>
                                <select id="speedSelect" onChange={event => this.setState({ speed: parseInt(event.target.value, 10) })} defaultValue={this.state.speed} disabled={this.state.connected}>
                                    {SPEED_OPTIONS.map(option => <option value={option} key={option}>{option.toLocaleString()}</option>)}
                                </select>
                            </div>
                            <div className="command">
                                {CONNECTION_FORM_COMMANDS.map((command, index) => <button key={index} onClick={command.onClick} disabled={command.disabled}>{command.label}</button>)}
                            </div>
                        </fieldset>

                        <CommandFieldset className="loading-form" title="Load" commands={LOAD_FORM_COMMANDS}>
                            <label htmlFor="binSelect">.bin file</label>
                            <select name="binSelect" id="binSelect" onChange={this.onBinSelectChange} defaultValue={this.state.bin} >
                                <option value=" ">Select .bin file</option>
                                {this.state.bins.map((bin, index) => <option value={bin} key={index}>{bin}</option>)}
                            </select>
                        </CommandFieldset>

                        <CommandFieldset className="run-form" title={'Execution: ' + this.calculProcessorSpeed() + ' Hz'} commands={RUN_FORM_COMMANDS}>
                            <label htmlFor="runSpeed">Run speed:</label>
                            <input type="range" name="runSpeed" id="runSpeed" min={RUN_SPEED_RANGE_OPTIONS[this.state.speedRange].min} max={RUN_SPEED_RANGE_OPTIONS[this.state.speedRange].max} onChange={this.onRunSpeedChange} value={this.state.runSpeed} />
                            <label htmlFor="speedRange">Range:</label>
                            <select name="speedRange" id="speedRange" onChange={this.onSpeedRangeChange}>
                                {RUN_SPEED_RANGE_OPTIONS.map((range, index) => <option value={index} key={index}>{range.label}</option>)}
                            </select>
                        </CommandFieldset>

                        <CommandFieldset className="step-form" title="Step by Step" commands={STEP_FORM_COMMANDS} >
                            <label htmlFor="stepCountSelect">Step count</label>
                            <select name="stepCountSelect" id="stepCountSelect" onChange={(event) => this.setState({ stepCount: event.target.value })}>
                                {STEP_COUNT_OPTIONS.map(option => <option value={option} defaultValue={this.state.stepCount} key={option}>{option}</option>)}
                            </select>
                        </CommandFieldset>
                    </section>
                </main>

            </div>
        )
    }
}

export default ALT6502
