#include <iostream>

// Same declaration found in arduino
typedef uint8_t byte;
typedef unsigned int word;

/**
 * 03       range total
 * 4000     start range 1
 * 4010     end range 1
 * 4010     start range 2   TODO: wrong address here
 * 401F     end range 2
 * 5000     start range 3
 * 501F     end range 3
 **/
const byte program[] = {0x03, 0x00, 0x40, 0x10, 0x40, 0x10, 0x40, 0x1F, 0x40, 0x00, 0x50, 0x1F, 0x50, 0xA9, 0xFF, 0x8D, 0x00, 0x40};

short getAddressRange(word address) {
    short range = -1;
    uint8_t rangeCounter = 0;

    while ((rangeCounter < program[0]) && (range == -1)) {
        // Each range is defined by start and end address
        const uint8_t offset = rangeCounter * 4;

        word startAddress = program[offset + 2];
        startAddress = (startAddress << 8) + program[offset + 1];

        word endAddress = program[offset + 4];
        endAddress = (endAddress << 8) + program[offset + 3];

        if (address >= startAddress && address <= endAddress) {
            range = rangeCounter;
        }

        /*         std::cout << "Addresses range:" << (short)rangeCounter << "\n"
                          << std::hex
                          << startAddress << "\n"
                          << endAddress
                          << std::endl; */

        rangeCounter++;
    }

    return range;
}

int main() {
    const int result = getAddressRange(0x5011);

    const word resetVector = 0x8000 + (program[0] * 4) + 1;

    std::cout << "Addresses range: " << result << "\n"
              << "Reset vector: " << std::hex << resetVector << std::endl;
    return 0;
}
