const fs = require('fs')
const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')

function initSerialPort (usbPort, baudRate, callback) {
    console.info('Opening serial connection:')
    console.info('  Port', usbPort)
    console.info('  Speed', baudRate)

    const serialPort = new SerialPort(usbPort, {
        baudRate: baudRate,
        autoOpen: false
    })

    serialPort.on('error', function (data) {
        console.error('Error: ' + data)
    })

    serialPort.open(function (error) {
        if (error) {
            console.error('Unable to open port:', error)
        } else {
            console.info('Port:', usbPort)

            const parser = serialPort.pipe(new Readline({ delimiter: '\n' }))
            parser.on('data', (data) => {
                const value = data.toString('utf-8')
                console.log('====', value)
            })

            setTimeout(function () {
                if (callback) {
                    callback(serialPort)
                }
            }, 2000)
        }
    })
}

function showPortList () {
    console.info('List active ports:')
    SerialPort.list().then(
        ports => ports.forEach(console.log),
        err => console.error(err)
    )
}

function executeUpload (config) {
    try {
        const command = Buffer.from('l') // todo
        const bin = fs.readFileSync(config.bin)
        const message = Buffer.concat([command, bin], command.length + bin.length)

        const baudRate = parseInt(config.speed, 10)

        initSerialPort(config.port, baudRate, function (serialPort) {
            serialPort.write(message, function (error) {
                if (error) {
                    console.error('Error writing on serial port: ', error.message)
                }
                console.log('Sending program:', message)
            })
        })
    } catch (error) {
        console.error('File not found:', config.bin)
    }
}

module.exports = {
    executeUpload,
    showPortList
}
