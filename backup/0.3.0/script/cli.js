
const path = require('path')
const fs = require('fs')
const { executeUpload, showPortList } = require('./loader')
const { startServer } = require('../src/server')

const ENCODING = 'utf-8'
const CONFIG_FILE_NAME = 'bin-loader.json'
const PREFIX_PARMETER = '--'
const INIT_PARAMETER = PREFIX_PARMETER + 'init'
const LIST_PARAMETER = PREFIX_PARMETER + 'list'
const PORT_PARAMETER = PREFIX_PARMETER + 'port='
const SPEED_PARAMETER = PREFIX_PARMETER + 'speed='
const BIN_PARAMETER = PREFIX_PARMETER + 'bin='

function generateConfigurationFile (configFilePath, config) {
    const option = {
        encoding: ENCODING
    }
    fs.writeFileSync(configFilePath, JSON.stringify(config, null, '    '), option)
}

/**
 * Initialize a default configuration object and overwrite it value
 * using a configuration file or command line parameters.
 *
 * @param {*} args
 */
function processCommandLine (args) {
    const configFilePath = path.join(process.cwd(), CONFIG_FILE_NAME)
    const configFileExist = fs.existsSync(configFilePath)

    let isReady = false
    let showHelp = true

    let config = {
        port: 'Undefined',
        speed: 115200,
        bin: 'file.bin'
    }

    if (configFileExist) {
        const configFile = fs.readFileSync(configFilePath, { encoding: ENCODING })
        config = JSON.parse(configFile)
        isReady = true
    }

    if (args.length > 0) {
        const parameters = args.filter(parameter => parameter.startsWith(PREFIX_PARMETER))

        if (parameters.includes(INIT_PARAMETER)) {
            if (configFileExist) {
                console.warn('Configuration file already exists: ', configFilePath)
            } else {
                generateConfigurationFile(configFilePath, config)
            }

            isReady = false
            showHelp = false
        } else if (parameters.includes(LIST_PARAMETER)) {
            showPortList()

            isReady = false
            showHelp = false
        } else {
            if (parameters.length === 3) {
                isReady = true
            }
            parameters.forEach((parameter) => {
                if (parameter.startsWith(PORT_PARAMETER)) {
                    config.port = parameter.split('=')[1]
                } else if (parameter.startsWith(SPEED_PARAMETER)) {
                    config.speed = parameter.split('=')[1]
                } else if (parameter.startsWith(BIN_PARAMETER)) {
                    config.bin = parameter.split('=')[1]
                }
            })
        }

        if (isReady) {
            executeUpload(config)
        } else if (showHelp) {
            console.warn('Configuration file not found:', configFilePath)
            console.info('Please provide the following parameters:')
            console.info('  --port=', 'PORT for serial communication.')
            console.info('  --speed=', 'Serial communication speed.')
            console.info('  --bin=', 'Binary file to upload')
            console.info('Or use --init to generate a configuration file.')
            console.info('Command line parameters override configuration file values.')
        }
    } else {
        startServer(process.cwd())
    }
}

module.exports = {
    processCommandLine
}
