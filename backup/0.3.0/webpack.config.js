const HtmlWebPackPlugin = require('html-webpack-plugin')
const path = require('path')

module.exports = {
    entry: {
        app: path.join(__dirname, 'src', 'client', 'index.jsx')
    },
    resolve: {
        extensions: ['.jsx', '.js', '.json'],
        alias: {
            component: path.resolve(__dirname, 'src/client/'),
            container: path.resolve(__dirname, 'src/server/')
        }
    },
    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist/client'),
        clean: true
    },
    devtool: 'inline-source-map',
    devServer: {
        open: true,
        static: [
            {
                directory: path.join(__dirname, 'src', 'client', 'style')
            }
        ]
    },
    module: {
        rules: [
            {
                test: /\.jsx$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.html$/,
                use: [{ loader: 'html-loader', options: { minimize: false } }]
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader']
            },

            {

                test: /\.(png|svg|jpg|jpeg|gif)$/i,

                type: 'asset/resource'

            }
        ]
    },
    plugins: [
        new HtmlWebPackPlugin({
            template: 'src/client/index.html',
            filename: './index.html'
        })
    ]

}
