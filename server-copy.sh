#!/bin/bash

local_file="."
remote_user="martin"
remote_host="192.168.1.4"
remote_path="/home/martin/alt6502/server"

scp $local_file/requirements.txt $local_file/src/server/*.py "$remote_user@$remote_host:$remote_path"

# scp ./src/server/*.py martin@192.168.1.4:/home/martin/alt6502/server

# Check if scp command succeeded
if [ $? -eq 0 ]; then
    echo "File copied successfully to remote server."
else
    echo "Error: File copy failed."
fi
