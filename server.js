const express = require('express')
const { createServer } = require('http')
const { Server } = require('socket.io')

const app = express()
const httpServer = createServer(app)
const io = new Server(httpServer, {
    cors: {
        origin: 'http://localhost:8080',
        methods: ['GET', 'POST']
    }
})

// NES resolution
const NES_WIDTH = 256
const NES_HEIGHT = 240
const videoAddresses = []

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
    next()
})

const initVideo = (width, height, content) => {
    const videos = []

    for (let w = 0; w < width; w++) {
        for (let h = 0; h < height; h++) {
            videos.push(content)
        }
    }

    return videos
}

io.on('connection', (socket) => {
    socket.emit('init-video', (videoAddresses.length > 0 ? videoAddresses : initVideo(NES_WIDTH, NES_HEIGHT, 0)))

    socket.on('ping', () => {
        socket.emit('pong', 'hey')
    })

    let address = 0

    setInterval(() => {
        videoAddresses[address] = '00FF'
        socket.emit('update-video', [[address, '00FF']])

        address++
    }, 33)

    // setInterval(() => {
    //    socket.emit('display', new Date().toISOString())
    // }, 33.33)
})

httpServer.listen(3000)
